package br.com.senac.test;

import br.com.senac.ConversorTemperatura;
import org.junit.Assert;
import org.junit.Test;

public class ConversorTest {

    @Test
    public void testandoConversaoParaCelsiusParaFahrenheitTest() {
        double fahrenheit = 212;
        double resultado = ConversorTemperatura.ConverterParaCelsius(fahrenheit);

        Assert.assertEquals(100, resultado, 0);
        /*
        if (resultado == 100) {
            System.out.println("Deu certo....");
        } else {
            System.out.println("Deu errado...");
        }
         */
    }

    @Test
    public void testandoConversaoParaFahrenheitParaCelsiusTest() {

        double celsius = 100;
        double resultado = ConversorTemperatura.ConverterParaFahrenheit(celsius);

        Assert.assertEquals(212, resultado, 0);
    }

}
