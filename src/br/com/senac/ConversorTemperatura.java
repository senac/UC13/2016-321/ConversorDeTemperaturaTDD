
package br.com.senac;


public class ConversorTemperatura {
    
    
    public static double ConverterParaFahrenheit(double celsius){
        return ((celsius * 9.0)/5.0) + 32 ; 
    }
    
    public static double ConverterParaCelsius(double fahrenheit ){
        return (5.0 * (fahrenheit - 32 ))/9 ; 
    }
    
}
